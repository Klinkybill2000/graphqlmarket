import React, { useCallback, useState } from 'react';
import { withStyle } from 'baseui';
import {
    Grid,
    Row as Rows,
    Col as Column,
} from '../../components/FlexBox/FlexBox';
import { useDrawerDispatch } from '../../context/DrawerContext';
import Select from '../../components/Select/Select';
import Input from '../../components/Input/Input';
import Button from '../../components/Button/Button';
import Checkbox from '../../components/CheckBox/CheckBox';
import gql from 'graphql-tag';
import { useQuery } from '@apollo/react-hooks';
import { Wrapper, Header, Heading } from '../../components/WrapperStyle';
import {
    TableWrapper,
    StyledTable,
    StyledHeadCell,
    StyledCell,
    ImageWrapper,
} from './Shop.style';

import {
    Plus,
    Accessories,
    BathOil,
    BeautyHealth,
    Beverage,
    Breakfast,
    Cooking,
    Dairy,
    Deodorent,
    Eyes,
    Face,
    FacialCare,
    FruitsVegetable,
    HandBags,
    HomeCleaning,
    LaptopBags,
    Lips,
    MeatFish,
    OralCare,
    OuterWear,
    Pants,
    PetCare,
    Purse,
    ShavingNeeds,
    Shirts,
    ShoulderBags,
    Skirts,
    Snacks,
    Tops,
    Wallet,
    WomenDress,
} from '../../components/AllSvgIcon';
import NoResult from '../../components/NoResult/NoResult';
let icons = {
    Accessories: Accessories,
    BathOil: BathOil,
    BeautyHealth: BeautyHealth,
    Beverage: Beverage,
    Breakfast: Breakfast,
    Cooking: Cooking,
    Dairy: Dairy,
    Deodorent: Deodorent,
    Eyes: Eyes,
    Face: Face,
    FacialCare: FacialCare,
    FruitsVegetable: FruitsVegetable,
    HandBags: HandBags,
    HomeCleaning: HomeCleaning,
    LaptopBags: LaptopBags,
    Lips: Lips,
    MeatFish: MeatFish,
    OralCare: OralCare,
    OuterWear: OuterWear,
    Pants: Pants,
    PetCare: PetCare,
    Purse: Purse,
    ShavingNeeds: ShavingNeeds,
    Shirts: Shirts,
    ShoulderBags: ShoulderBags,
    Skirts: Skirts,
    Snacks: Snacks,
    Tops: Tops,
    Wallet: Wallet,
    WomenDress: WomenDress,
};
const GET_SHOPS = gql`
  query getShops($searchBy: String) {
    shops(searchBy: $searchBy) {
      id
      icon
      name
      location
      description
    }
  }
`;

const Col = withStyle(Column, () => ({
    '@media only screen and (max-width: 767px)': {
        marginBottom: '20px',

        ':last-child': {
            marginBottom: 0,
        },
    },
}));

const Row = withStyle(Rows, () => ({
    '@media only screen and (min-width: 768px)': {
        alignItems: 'center',
    },
}));

const shopSelectOptions = [
    { value: 'grocery', label: 'Grocery' },
    { value: 'women-cloths', label: 'Women Cloth' },
    { value: 'bags', label: 'Bags' },
    { value: 'makeup', label: 'Makeup' },
];

export default function Shop() {
    const [shop, setShop] = useState([]);
    const [search, setSearch] = useState('');
    const dispatch = useDrawerDispatch();
    const [checkedId, setCheckedId] = useState([]);
    const [checked, setChecked] = useState(false);
    const openDrawer = useCallback(
        () => dispatch({ type: 'OPEN_DRAWER', drawerComponent: 'SHOP_FORM' }),
        [dispatch]
    );

    const { data, error, refetch } = useQuery(GET_SHOPS);

    if (error) {
        return <div>Error! {error.message}</div>;
    }
    function handleSearch(event) {
        const value = event.currentTarget.value;
        setSearch(value);
        refetch({
            type: shop.length ? shop[0].value : null,
            searchBy: value,
        });
    }
    function handleShop({ value }) {
        setShop(value);
        if (value.length) {
            refetch({
                type: value[0].value,
            });
        } else {
            refetch({
                type: null,
            });
        }
    }

    function onAllCheck(event) {
        if (event.target.checked) {
            const idx = data && data.shops.map(shop => shop.id);
            setCheckedId(idx);
        } else {
            setCheckedId([]);
        }
        setChecked(event.target.checked);
    }

    function handleCheckbox(event) {
        const { name } = event.currentTarget;
        if (!checkedId.includes(name)) {
            setCheckedId(prevState => [...prevState, name]);
        } else {
            setCheckedId(prevState => prevState.filter(id => id !== name));
        }
    }
    function Icon({ icon }) {
        const Component = icons.hasOwnProperty(icon) ? icons[icon] : 'span';
        return <Component />;
    }
    return (
        <Grid fluid={true}>
            <Row>
                <Col md={12}>
                    <Header
                        style={{
                            marginBottom: 30,
                            boxShadow: '0 0 5px rgba(0, 0 ,0, 0.05)',
                        }}
                    >
                        <Col md={2}>
                            <Heading>Shop</Heading>
                        </Col>

                        <Col md={10}>
                            <Row>
                                <Col md={3} lg={3}>
                                    {/*<Select*/}
                                    {/*    options={shopSelectOptions}*/}
                                    {/*    labelKey='label'*/}
                                    {/*    valueKey='value'*/}
                                    {/*    placeholder='Shop Type'*/}
                                    {/*    value={shop}*/}
                                    {/*    searchable={false}*/}
                                    {/*    onChange={handleShop}*/}
                                    {/*/>*/}
                                </Col>

                                <Col md={5} lg={6}>
                                    <Input
                                        value={search}
                                        placeholder='Ex: Search By Name'
                                        onChange={handleSearch}
                                        clearable
                                    />
                                </Col>

                                <Col md={4} lg={3}>
                                    <Button
                                        onClick={openDrawer}
                                        startEnhancer={() => <Plus />}
                                        overrides={{
                                            BaseButton: {
                                                style: () => ({
                                                    width: '100%',
                                                    borderTopLeftRadius: '3px',
                                                    borderTopRightRadius: '3px',
                                                    borderBottomLeftRadius: '3px',
                                                    borderBottomRightRadius: '3px',
                                                }),
                                            },
                                        }}
                                    >
                                        Add Shop
                                    </Button>
                                </Col>
                            </Row>
                        </Col>
                    </Header>

                    <Wrapper style={{ boxShadow: '0 0 5px rgba(0, 0 , 0, 0.05)' }}>
                        <TableWrapper>
                            <StyledTable $gridTemplateColumns='minmax(70px, 70px) minmax(70px, 70px) minmax(70px, 70px) minmax(150px, auto) minmax(150px, auto) auto'>
                                <StyledHeadCell>
                                    <Checkbox
                                        type='checkbox'
                                        value='checkAll'
                                        checked={checked}
                                        onChange={onAllCheck}
                                        overrides={{
                                            Checkmark: {
                                                style: {
                                                    borderWidth: '2px',
                                                    borderRadius: '4px',
                                                },
                                            },
                                        }}
                                    />
                                </StyledHeadCell>
                                <StyledHeadCell>Id</StyledHeadCell>
                                <StyledHeadCell>Image</StyledHeadCell>
                                <StyledHeadCell>Name</StyledHeadCell>
                                <StyledHeadCell>Location</StyledHeadCell>
                                <StyledHeadCell>Description</StyledHeadCell>

                                {data ? (
                                    data.shops.length ? (
                                        data.shops
                                            .map(item => Object.values(item))
                                            .map((row, index) => (
                                                <React.Fragment key={index}>
                                                    <StyledCell>
                                                        <Checkbox
                                                            name={row[0]}
                                                            checked={checkedId.includes(row[0])}
                                                            onChange={handleCheckbox}
                                                            overrides={{
                                                                Checkmark: {
                                                                    style: {
                                                                        borderWidth: '2px',
                                                                        borderRadius: '4px',
                                                                    },
                                                                },
                                                            }}
                                                        />
                                                    </StyledCell>
                                                    <StyledCell>{row[0]}</StyledCell>
                                                    <StyledCell>
                                                        <ImageWrapper>
                                                            <Icon icon={row[1]} />
                                                        </ImageWrapper>
                                                    </StyledCell>
                                                    <StyledCell>{row[2]}</StyledCell>
                                                    <StyledCell>{row[3]}</StyledCell>
                                                    <StyledCell>{row[4]}</StyledCell>
                                                </React.Fragment>
                                            ))
                                    ) : (
                                        <NoResult
                                            hideButton={false}
                                            style={{
                                                gridColumnStart: '1',
                                                gridColumnEnd: 'one',
                                            }}
                                        />
                                    )
                                ) : null}
                            </StyledTable>
                        </TableWrapper>
                    </Wrapper>
                </Col>
            </Row>
        </Grid>
    );
}
