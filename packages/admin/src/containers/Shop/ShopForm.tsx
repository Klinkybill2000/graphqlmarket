import React, { useState, useCallback } from 'react';
import { useForm } from 'react-hook-form';
import uuidv4 from 'uuid/v4';
import { useMutation } from '@apollo/react-hooks';
import { Scrollbars } from 'react-custom-scrollbars';
import {
    Form,
    DrawerTitleWrapper,
    DrawerTitle,
    FieldDetails,
    ButtonGroup,
} from '../DrawerItems/DrawerItems.style';
import { FormFields, FormLabel } from '../../components/FormFields/FormFields';
import {Col, Row} from "../../components/FlexBox/FlexBox";
import DrawerBox from "../../components/DrawerBox/DrawerBox";
import Uploader from "../../components/Uploader/Uploader";
import Input from "../../components/Input/Input";
import Button, {KIND} from "../../components/Button/Button";
import {useDrawerDispatch} from "../../context/DrawerContext";
import gql from "graphql-tag";

const GET_SHOPS = gql`
  query getShops($searchBy: String) {
    shops(searchBy: $searchBy) {
      id
      icon
      name
      location
      description
    }
  }
`;
const CREATE_SHOP = gql`
  mutation createShop($shop: AddShopInput!) {
    createShop(shop: $shop) {
      id
      name
      icon
      creation_date
      location
      description
    }
  }
`;

const options = [
    { value: 'grocery', name: 'Grocery', id: '1' },
    { value: 'women-cloths', name: 'Women Cloths', id: '2' },
    { value: 'bags', name: 'Bags', id: '3' },
    { value: 'makeup', name: 'Makeup', id: '4' },
];
type Props = any;

const AddShop: React.FC<Props> = props => {
    const dispatch = useDrawerDispatch();
    const closeDrawer = useCallback(() => dispatch({ type: 'CLOSE_DRAWER' }), [
        dispatch,
    ]);
    const { register, handleSubmit, setValue } = useForm();
    const [shop, setShop] = useState([]);
    React.useEffect(() => {
        register({ name: 'parent' });
        register({ name: 'image' });
    }, [register]);
    const [createShop] = useMutation(CREATE_SHOP, {
        update(cache, { data: { createShop } }) {
            const { shops } = cache.readQuery({
                query: GET_SHOPS,
            });

            cache.writeQuery({
                query: GET_SHOPS,
                data: { shops: shops.concat([createShop]) },
            });
        },
    });

    const onSubmit = ({ name, location, description, image }) => {
        const newShop = {
            id: uuidv4(),
            name: name,
            icon: image,
            location: location,
            description: description,
            creation_date: new Date(),
        };
        createShop({
            variables: { shop: newShop },
        });
        closeDrawer();
        console.log(newShop, 'newShop');
    };
    const handleChange = ({ value }) => {
        setValue('parent', value);
        setShop(value);
    };
    const handleUploader = files => {
        setValue('image', files[0].path);
    };

    return (
        <>
            <DrawerTitleWrapper>
                <DrawerTitle>Add Shop</DrawerTitle>
            </DrawerTitleWrapper>

            <Form onSubmit={handleSubmit(onSubmit)} style={{ height: '100%' }}>
                <Scrollbars
                    autoHide
                    renderView={props => (
                        <div {...props} style={{ ...props.style, overflowX: 'hidden' }} />
                    )}
                    renderTrackHorizontal={props => (
                        <div
                            {...props}
                            style={{ display: 'none' }}
                            className="track-horizontal"
                        />
                    )}
                >
                    <Row>
                        <Col lg={4}>
                            <FieldDetails>Upload your Shop image here</FieldDetails>
                        </Col>
                        <Col lg={8}>
                            <DrawerBox
                                overrides={{
                                    Block: {
                                        style: {
                                            width: '100%',
                                            height: 'auto',
                                            padding: '30px',
                                            borderRadius: '3px',
                                            backgroundColor: '#ffffff',
                                            display: 'flex',
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                        },
                                    },
                                }}
                            >
                                <Uploader onChange={handleUploader} />
                            </DrawerBox>
                        </Col>
                    </Row>

                    <Row>
                        <Col lg={4}>
                            <FieldDetails>
                                Add your shop description and necessary informations from
                                here
                            </FieldDetails>
                        </Col>

                        <Col lg={8}>
                            <DrawerBox>
                                <FormFields>
                                    <FormLabel>Shop Name</FormLabel>
                                    <Input
                                        inputRef={register({ required: true, maxLength: 20 })}
                                        name="name"
                                    />
                                </FormFields>

                                <FormFields>
                                    <FormLabel>Location</FormLabel>
                                    {/*pattern: /^[A-Za-z]+$/i */}
                                    <Input
                                        inputRef={register({ required: true})}
                                        name="location"
                                    />
                                </FormFields>

                                <FormFields>
                                    <FormLabel>Description</FormLabel>
                                    <Input
                                        inputRef={register({required: true })}
                                        name="description"
                                    />
                                </FormFields>

                                {/*<FormFields>*/}
                                {/*    <FormLabel>Parent</FormLabel>*/}
                                {/*    <Select*/}
                                {/*        options={options}*/}
                                {/*        labelKey="name"*/}
                                {/*        valueKey="value"*/}
                                {/*        placeholder="Ex: Choose parent category"*/}
                                {/*        value={category}*/}
                                {/*        searchable={false}*/}
                                {/*        onChange={handleChange}*/}
                                {/*        overrides={{*/}
                                {/*            Placeholder: {*/}
                                {/*                style: ({ $theme }) => {*/}
                                {/*                    return {*/}
                                {/*                        ...$theme.typography.fontBold14,*/}
                                {/*                        color: $theme.colors.textNormal,*/}
                                {/*                    };*/}
                                {/*                },*/}
                                {/*            },*/}
                                {/*            DropdownListItem: {*/}
                                {/*                style: ({ $theme }) => {*/}
                                {/*                    return {*/}
                                {/*                        ...$theme.typography.fontBold14,*/}
                                {/*                        color: $theme.colors.textNormal,*/}
                                {/*                    };*/}
                                {/*                },*/}
                                {/*            },*/}
                                {/*            OptionContent: {*/}
                                {/*                style: ({ $theme, $selected }) => {*/}
                                {/*                    return {*/}
                                {/*                        ...$theme.typography.fontBold14,*/}
                                {/*                        color: $selected*/}
                                {/*                            ? $theme.colors.textDark*/}
                                {/*                            : $theme.colors.textNormal,*/}
                                {/*                    };*/}
                                {/*                },*/}
                                {/*            },*/}
                                {/*            SingleValue: {*/}
                                {/*                style: ({ $theme }) => {*/}
                                {/*                    return {*/}
                                {/*                        ...$theme.typography.fontBold14,*/}
                                {/*                        color: $theme.colors.textNormal,*/}
                                {/*                    };*/}
                                {/*                },*/}
                                {/*            },*/}
                                {/*            Popover: {*/}
                                {/*                props: {*/}
                                {/*                    overrides: {*/}
                                {/*                        Body: {*/}
                                {/*                            style: { zIndex: 5 },*/}
                                {/*                        },*/}
                                {/*                    },*/}
                                {/*                },*/}
                                {/*            },*/}
                                {/*        }}*/}
                                {/*    />*/}
                                {/*</FormFields>*/}
                            </DrawerBox>
                        </Col>
                    </Row>
                </Scrollbars>

                <ButtonGroup>
                    <Button
                        kind={KIND.minimal}
                        onClick={closeDrawer}
                        overrides={{
                            BaseButton: {
                                style: ({ $theme }) => ({
                                    width: '50%',
                                    borderTopLeftRadius: '3px',
                                    borderTopRightRadius: '3px',
                                    borderBottomRightRadius: '3px',
                                    borderBottomLeftRadius: '3px',
                                    marginRight: '15px',
                                    color: $theme.colors.red400,
                                }),
                            },
                        }}
                    >
                        Cancel
                    </Button>

                    <Button
                        type="submit"
                        overrides={{
                            BaseButton: {
                                style: ({ $theme }) => ({
                                    width: '50%',
                                    borderTopLeftRadius: '3px',
                                    borderTopRightRadius: '3px',
                                    borderBottomRightRadius: '3px',
                                    borderBottomLeftRadius: '3px',
                                }),
                            },
                        }}
                    >
                        Create Shop
                    </Button>
                </ButtonGroup>
            </Form>
        </>
    );
};

export default AddShop;
