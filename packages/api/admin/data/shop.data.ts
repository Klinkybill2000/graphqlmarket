import { plainToClass } from "class-transformer";
import Shop from "../../src/entity/Shop";

const loadShops = (): Shop[] => {
    return plainToClass(Shop, [
        {
            id: 1,
            name: "Shop no 1",
            location: "Germany 1 ",
            description: "This is my shop",
            icon: "FruitsVegetable",
            type: "one"
        },
        {
            id: 2,
            name: "Shop no 2",
            location: "Germany 2 ",
            description: "This is my shop",
            icon: "FruitsVegetable",
            type: "one"
        },

        {
            id: 3,
            name: "Shop no 3",
            location: "Germany 3 ",
            description: "This is my shop",
            icon: "FruitsVegetable",
            type: "one"
        },

        {
            id: 4,
            name: "Shop no 4",
            location: "Germany 4 ",
            description: "This is my shop",
            icon: "FruitsVegetable",
            type: "one"
        },

        {
            id: 5,
            name: "Shop no 5",
            location: "Germany 5 ",
            description: "This is my shop",
            icon: "FruitsVegetable",
            type: "one"
        },
    ]);
};

export default loadShops;
