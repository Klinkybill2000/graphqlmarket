import { InputType, Field, ID } from 'type-graphql';
import Shop from "../../../src/entity/Shop";

@InputType({ description: 'New Shop Data' })
export default class AddShopInput implements Partial<Shop> {
    @Field(type => ID)
    id: string;

    @Field()
    name: string;

    @Field({ defaultValue: null })
    value: string;

    @Field({ nullable: true })
    icon: string;

    @Field({ nullable: true })
    location: string;

    @Field({ nullable: true })
    description: string;

    // @Field({ nullable: true })
    // number_of_product?: number;

    @Field({ nullable: true })
    creation_date: Date;
}
