import {Arg, Mutation, Query, Resolver} from 'type-graphql';
import search from "../../helpers/search";
import Shop from "../../../src/entity/Shop";
import AddShopInput from "./shop.input_type";

@Resolver()
export default class ShopResolver {

    @Query(returns => [Shop], {description: 'Get all the shops'})
    async shops(
        @Arg('searchBy', {defaultValue: ''}) searchBy?: string
    ): Promise<Shop[]> {
        let shops = await Shop.find();
        return await search(shops, ['name'], searchBy);
    }

    @Query(returns => Shop)
    async shop(@Arg('id') id: string
    ): Promise<Shop | undefined> {
        return await Shop.findOne({where: {id}}).catch()
    }


    // Register mutation to create a new user in the USER DB
    // Validations and structure of the input is being read from RegisterInput
    // We are spreading out RegisterInput to read the fields
    @Mutation(() => Shop, {description: 'Create Shop'})
    async createShop(@Arg('shop')
            {
                id,
                name,
                icon,
                location,
                creation_date,
                description
            }: AddShopInput
    ): Promise<Shop> {
        console.log(name, 'shop');

        // .create() creates an User object
        // ,save() created an entry in the DB
        // Just calling .create() does not update the DB

        return await Shop.create({
            id,
            name,
            icon,
            location,
            description,
            creation_date
        }).save().catch();
    }
}
