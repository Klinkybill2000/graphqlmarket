import { Resolver, Query, Arg, ID, Mutation } from 'type-graphql';
import loadShops from '../../data/shop.data';
import Shop from '../../../src/entity/Shop';
import AddShopInput from './shop.input_type';
import search from '../../helpers/search';
@Resolver()
export default class ShopResolverCopy {
    //if something happens to main shop class
    //this class only showing demo data

    private readonly shopsCollection: Shop[] = loadShops();

    @Query(returns => [Shop], { description: 'Get all the shops' })
    async shops(
        @Arg('searchBy', { defaultValue: '' }) searchBy?: string
    ): Promise<Shop[]> {
        let shops = this.shopsCollection;
        return await search(shops, ['name'], searchBy);
    }

    @Query(returns => Shop)
    async shop(
        @Arg('id', type => ID) id: string
    ): Promise<Shop | undefined> {
        return await this.shopsCollection.find(shop => shop.id === id);
    }

    @Mutation(() => Shop, { description: 'Create Shop' })
    async createShop(
        @Arg('shop') shop: AddShopInput
    ): Promise<Shop> {
        console.log(shop, 'shop');

        return await shop;
    }
}
