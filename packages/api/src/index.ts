import "reflect-metadata";
import express from "express";
import {ApolloServer} from "apollo-server-express";
import {importSchema} from "graphql-import"
import {resolvers} from "./resolvers";
import * as path2 from "path";
import {createConnection} from "typeorm";

const typeDefs = importSchema(path2.join(__dirname, "./schema.graphql"));

const app: express.Application = express();
const path = '/admin/graphql';
const PORT = process.env.PORT || 4000;

const main = async () => {
    const apolloServer = new ApolloServer({
        typeDefs,
        resolvers
    });

    //synchronize database connection
    createConnection().then(connection => {
        //after connection create server
        apolloServer.applyMiddleware({app, path});

        app.listen(PORT, () => {
            console.log(`🚀 started http://localhost:${PORT}${path}`);
        });
    });
};

main();

