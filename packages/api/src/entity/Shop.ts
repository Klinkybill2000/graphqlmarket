import {ObjectType, Field, Int, ID} from 'type-graphql';
import {BaseEntity, Column, Entity, PrimaryColumn} from "typeorm";

@ObjectType()
@Entity("shops")
export default class Shop extends BaseEntity{
    @Field(type => ID)
    @PrimaryColumn()
    id: string;

    @Field(type => String)
    @Column()
    name: string;

    @Field(type => String)
    @Column()
    location: string;

    @Field(type => String)
    @Column()
    icon: string;

    @Field(type => String)
    @Column()
    description: string;

    @Field()
    @Column()
    creation_date: Date;
}